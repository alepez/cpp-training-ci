#include <spdlog/spdlog.h>

#include <gol/app.hpp>
#include <gol/canvas_factory.hpp>
#include <gol/parse.hpp>

using namespace alepez;

int main(int argc, const char** argv) {
    spdlog::info("Starting Game of Life");

    Options options {};
    options.width = parse_arg_or_fallback<int>(argc, argv, 1, 640);
    options.height = parse_arg_or_fallback<int>(argc, argv, 2, 480);
    options.seed = parse_arg_or_fallback<int>(argc, argv, 3, 1);
    options.scale = parse_arg_or_fallback<int>(argc, argv, 4, 1);
    options.canvas_type = CanvasType::Sdl;

    App app {options};
    app.run();

    return 0;
}
