#include <gol/app.hpp>
#include <gol/parse.hpp>

using namespace alepez;

int main(int argc, const char** argv) {
    Options options {};
    options.width = parse_arg_or_fallback<int>(argc, argv, 1, 80);
    options.height = parse_arg_or_fallback<int>(argc, argv, 2, 20);
    options.seed = parse_arg_or_fallback<int>(argc, argv, 3, 1);
    options.scale = 1;
    options.canvas_type = CanvasType::Terminal;

    App app {options};
    app.run();

    return 0;
}
