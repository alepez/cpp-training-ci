#include <ApprovalTests.hpp>
#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>
#include <gol/grid.hpp>
#include <gol/term_canvas.hpp>
#include <sstream>

using namespace alepez;

TEST_CASE("TermCanvas to stdout (manual test)", "[.manual]") {
    int width = 20;
    int height = 15;

    Grid grid {width, height};
    grid.at(7, 6).state = CellState::Alive;
    grid.at(7, 8).state = CellState::Alive;
    grid.at(7, 7).state = CellState::Alive;
    grid.at(6, 7).state = CellState::Alive;
    grid.at(8, 7).state = CellState::Alive;

    TermCanvas canvas {width, height};
    canvas.draw(grid);
}

TEST_CASE("TermCanvas can print to any ostream") {
    // Arrange
    int width = 7;
    int height = 7;
    std::stringstream out_stream;
    TermCanvas canvas {width, height, out_stream};

    // Act
    Grid grid {7, 7};
    grid.at(3, 2).state = CellState::Alive;
    grid.at(3, 4).state = CellState::Alive;
    grid.at(3, 3).state = CellState::Alive;
    grid.at(2, 3).state = CellState::Alive;
    grid.at(4, 3).state = CellState::Alive;
    canvas.draw(grid);

    // Assert
    const char* expected =
        "\x1B[H\x1B[J"
        ".......\n"
        ".......\n"
        "...#...\n"
        "..###..\n"
        "...#...\n"
        ".......\n"
        ".......\n\n";

    REQUIRE(out_stream.str() == expected);
}

TEST_CASE("TermCanvas can print to any ostream (approval test)") {
    // Arrange
    int width = 7;
    int height = 7;
    std::stringstream out_stream;
    TermCanvas canvas {width, height, out_stream};

    // Act
    Grid grid {7, 7};
    grid.at(3, 2).state = CellState::Alive;
    grid.at(3, 4).state = CellState::Alive;
    grid.at(3, 3).state = CellState::Alive;
    grid.at(2, 3).state = CellState::Alive;
    grid.at(4, 3).state = CellState::Alive;
    canvas.draw(grid);

    // Assert
    // Instead of writing the expected string directly, we can use ApprovalTests
    // to compare the output with a previously approved output.
    ApprovalTests::Approvals::verify(out_stream.str());
}

TEST_CASE("floating point") {
    float x = 1.0 / 3.0;
    REQUIRE(Catch::Approx(0.333).epsilon(0.01) == x);
}