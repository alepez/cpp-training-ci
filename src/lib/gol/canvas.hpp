#pragma once

#include "fwd.hpp"

namespace alepez {

// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class Canvas {
  public:
    virtual bool draw(const Grid& grid) = 0;
    virtual ~Canvas() = default;
};

}  // namespace alepez