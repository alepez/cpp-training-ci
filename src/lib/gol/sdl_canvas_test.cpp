#if FEATURE_SDL

#include <catch2/catch_test_macros.hpp>
#include <gol/sdl_canvas.hpp>

using namespace alepez;

TEST_CASE("SdlCanvas", "[.manual]") {
    int width = 40;
    int height = 30;

    Grid grid {width, height};
    grid.at(10, 9).state = CellState::Alive;
    grid.at(10, 11).state = CellState::Alive;
    grid.at(10, 10).state = CellState::Alive;
    grid.at(9, 10).state = CellState::Alive;
    grid.at(11, 10).state = CellState::Alive;

    SdlCanvas canvas {width, height, 20};
    canvas.draw(grid);
    getchar();

    // TODO Test manuale
    // Sono costretto a mettere getchar() per fare in modo che il test aspetti
    // che io possa vedere il risultato.
}

TEST_CASE("SdlCanvas draw_to_pixels") {
    int width = 20;
    int height = 15;
    int scale = 2;

    int win_width = width * scale;
    int win_height = height * scale;

    Grid grid {width, height};

    int x = 10;
    int y = 5;
    grid.at(x, y).state = CellState::Alive;

    std::vector<int> px(static_cast<std::size_t>(win_width * win_height));
    SdlCanvas::draw_to_pixels(px.data(), win_width, win_height, grid);

    int win_x = (x * scale);
    int win_y = y * scale;

    REQUIRE(px[0] == 0x00000000);
    REQUIRE(px[win_x + win_y * win_width] == 0x00FFFFFF);
    REQUIRE(px[win_x + 1 + win_y * win_width] == 0x00FFFFFF);
    REQUIRE(px[win_x + (win_y + 1) * win_width] == 0x00FFFFFF);
    REQUIRE(px[win_x + 1 + (win_y + 1) * win_width] == 0x00FFFFFF);

    // TODO Test troppo complesso. E conosce troppi dettagli dell'implementazione
}

#endif