#include <array>
#include <catch2/catch_test_macros.hpp>
#include <gol/parse.hpp>

using alepez::parse_arg_or_fallback;
using alepez::parse_or_fallback;

TEST_CASE("parse_or_fallback can parse int") {
    REQUIRE(parse_or_fallback<int>("42", 0) == 42);
    REQUIRE(parse_or_fallback<int>("-99", 0) == -99);
    REQUIRE(parse_or_fallback<int>("0", 99) == 0);
}

TEST_CASE("parse_or_fallback return fallback value when cannot parse int") {
    REQUIRE(parse_or_fallback<int>("", 42) == 42);
    REQUIRE(parse_or_fallback<int>("A", 43) == 43);
    REQUIRE(parse_or_fallback<int>("-A9", 44) == 44);
}

TEST_CASE("parse_arg_or_fallback can parse int") {
    std::array<const char*, 2> argv = {"640", "480"};
    REQUIRE(parse_arg_or_fallback<int>(argv.size(), argv.data(), 0, 99) == 640);
    REQUIRE(parse_arg_or_fallback<int>(argv.size(), argv.data(), 1, 99) == 480);
}

TEST_CASE(
    "parse_arg_or_fallback return fallback value when argument is missing") {
    std::array<const char*, 1> argv = {"640"};
    REQUIRE(parse_arg_or_fallback<int>(argv.size(), argv.data(), 0, 99) == 640);
    REQUIRE(parse_arg_or_fallback<int>(argv.size(), argv.data(), 1, 99) == 99);
}