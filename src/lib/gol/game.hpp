#pragma once

#include <random>
#include <vector>

#include "grid.hpp"

namespace alepez {

class Game {
  public:
    Game(int width, int height, int seed);
    void update_generation();
    [[nodiscard]] const Grid& grid() const;

  private:
    int count_adjacent_alive_cells(int i, int j);
    Grid m_grid;
    Grid m_grid_tmp;
};

}  // namespace alepez