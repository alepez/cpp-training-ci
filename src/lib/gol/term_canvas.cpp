#include "term_canvas.hpp"

#include <fmt/core.h>

#include <iostream>

#include "grid.hpp"

namespace alepez {

TermCanvas::TermCanvas(int width, int height, std::ostream& stream) :
    m_width {width},
    m_height {height},
    m_stream {stream} {}

TermCanvas::TermCanvas(int width, int height) :
    m_width {width},
    m_height {height},
    m_stream {std::cout} {}

bool TermCanvas::draw(const Grid& grid) {
    m_stream << "\033[H\033[J";

    for (int y = 0; y < m_height; ++y) {
        for (int x = 0; x < m_width; ++x) {
            bool is_alive = grid.at(x, y).state == CellState::Alive;
            if (is_alive) {
                m_stream << "#";
            } else {
                m_stream << ".";
            }
        }
        m_stream << "\n";
    }

    m_stream << std::endl;

    return true;
}

}  // namespace alepez