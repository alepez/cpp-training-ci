#include "app.hpp"

#include <thread>

#include "canvas.hpp"
#include "canvas_factory.hpp"
#include "game.hpp"

namespace alepez {

App::App(const Options& options) :
    m_game {
        std::make_unique<Game>(options.width, options.height, options.seed)},
    m_canvas {make_canvas(options)} {}

App::~App() = default;

void App::run() {
    m_running = true;

    while (m_running) {
        loop();
        std::this_thread::sleep_for(std::chrono::milliseconds {40});
    }
}

void App::loop() {
    m_game->update_generation();
    m_running = m_canvas->draw(m_game->grid());
}

}  // namespace alepez