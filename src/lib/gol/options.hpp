#pragma once

#include "canvas_factory.hpp"

namespace alepez {

struct Options {
    int width;
    int height;
    int seed;
    CanvasType canvas_type;
    int scale;
};

}  // namespace alepez