#pragma once

#include <memory>

#include "fwd.hpp"

namespace alepez {

enum class CanvasType {
    Terminal,
    Sdl,
};

std::unique_ptr<Canvas> make_canvas(const Options& options);

}  // namespace alepez