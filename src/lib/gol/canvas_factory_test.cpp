#include <catch2/catch_test_macros.hpp>
#include <gol/canvas.hpp>
#include <gol/canvas_factory.hpp>
#include <gol/options.hpp>

using alepez::CanvasType;
using alepez::Options;

static const Options DEFAULT_OPTIONS = {
    40,
    30,
    1,
    CanvasType::Terminal,
    20,
};

TEST_CASE("CanvasFactory can create TermCanvas") {
    Options options = DEFAULT_OPTIONS;
    options.canvas_type = CanvasType::Terminal;
    auto canvas = make_canvas(options);
    REQUIRE(canvas != nullptr);
}