#pragma once

#if FEATURE_SDL

#include <SDL2/SDL_video.h>

#include <memory>

#include "canvas.hpp"
#include "grid.hpp"

namespace alepez {

class SdlCanvas: public Canvas {
  public:
    static void
    draw_to_pixels(int* pixels, int win_w, int win_h, const Grid& grid);

    SdlCanvas(int width, int height, int scale);
    ~SdlCanvas() override;
    SdlCanvas(const SdlCanvas&) = delete;
    SdlCanvas(SdlCanvas&&) = delete;
    SdlCanvas& operator=(const SdlCanvas&) = delete;
    SdlCanvas& operator=(SdlCanvas&&) = delete;

    bool draw(const Grid& grid) override;

  private:
    SDL_Window* m_win;
};

}  // namespace alepez

#endif