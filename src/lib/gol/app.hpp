#pragma once

#include <memory>

#include "fwd.hpp"
#include "options.hpp"

namespace alepez {

// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class App {
  public:
    explicit App(const Options& options);
    void run();
    ~App();

  private:
    void loop();

    bool m_running {};
    std::unique_ptr<Game> m_game;
    std::unique_ptr<Canvas> m_canvas;
};

}  // namespace alepez