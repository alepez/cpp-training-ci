#include "game.hpp"

#include <random>

namespace alepez {

static void random_init(Grid& grid, int seed);

inline int operator+(int l, Cell r) {
    return l + (r.state == CellState::Alive);
}

inline CellState cell_state(bool alive) {
    return alive ? CellState::Alive : CellState::Dead;
}

Game::Game(int width, int height, int seed) :
    m_grid {width, height},
    m_grid_tmp {width, height} {
    /// Use seed=0 when no random initialization is needed
    if (seed != 0) {
        random_init(m_grid, seed);
    }
}

void Game::update_generation() {
    auto h = m_grid.height();
    auto w = m_grid.width();

    for (int x = 0; x < w; ++x) {
        for (int y = 0; y < h; ++y) {
            const int adjacent_count = count_adjacent_alive_cells(x, y);
            const bool is_alive = m_grid.at(x, y).state == CellState::Alive;
            const bool will_be_alive =
                (adjacent_count == 3) || ((adjacent_count == 2 && is_alive));

            auto& cell = m_grid_tmp.at(x, y);
            cell.state = cell_state(will_be_alive);
        }
    }

    m_grid.swap(m_grid_tmp);
}

int Game::count_adjacent_alive_cells(int i, int j) {
    return 0 +  //
        m_grid.at(i - 1, j - 1) +  //
        m_grid.at(i - 1, j + 0) +  //
        m_grid.at(i - 1, j + 1) +  //
        m_grid.at(i + 0, j - 1) +  //
        m_grid.at(i + 0, j + 1) +  //
        m_grid.at(i + 1, j - 1) +  //
        m_grid.at(i + 1, j + 0) +  //
        m_grid.at(i + 1, j + 1);  //
}

[[nodiscard]] const Grid& Game::grid() const {
    return m_grid;
}

static void random_init(Grid& grid, int seed) {
    std::mt19937 gen(seed);
    std::uniform_int_distribution<> distrib(0, 1);

    auto h = grid.height();
    auto w = grid.width();

    for (int x = 0; x < w; ++x) {
        for (int y = 0; y < h; ++y) {
            const bool is_alive = distrib(gen);
            grid.at(x, y).state = cell_state(is_alive);
        }
    }
}

}  // namespace alepez