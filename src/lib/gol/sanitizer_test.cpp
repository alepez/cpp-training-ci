#include <fmt/core.h>
#include <spdlog/spdlog.h>

#include <array>
#include <catch2/catch_test_macros.hpp>

double division(double x, double y);

double get_item(double* arr, int index);

TEST_CASE("Division by zero", "[.undefined_behavior_sanitizer_should_fail]") {
    // ubsan should detect a division by zero
    auto result = division(1.0, 0.0) != 1.0;
    SPDLOG_INFO("{}", result);
}

TEST_CASE("Invalid memory access", "[.address_sanitizer_should_fail]") {
    // asan should detect an invalid memory access
    std::array<double, 2> arr = {1.0, 2.0};
    auto data = &arr[0];
    int index = 2 + (rand() % 2);
    auto result = get_item(data, index) != 0.0;
    SPDLOG_INFO("{}", result);
}

TEST_CASE("Uninitialized value, leaked", "[.memory_sanitizer_should_fail]") {
    // msan should detect an invalid memory access
    auto uninitialized = new int;
    auto result = *uninitialized == 0;
    SPDLOG_INFO("{}", result);
}

double division(double x, double y) {
    return x / y;
}

double get_item(double* arr, int index) {
    return arr[index];
}