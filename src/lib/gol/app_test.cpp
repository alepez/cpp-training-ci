#include <catch2/catch_test_macros.hpp>
#include <gol/app.hpp>

// This tells Approval Tests to provide a main() - only do this in one cpp file
#define APPROVALS_CATCH2_V3
#include <ApprovalTests.hpp>

using alepez::App;
using alepez::CanvasType;
using alepez::Options;

TEST_CASE("App can run forever", "[.forever]") {
    Options options {};
    options.width = 640;
    options.height = 480;
    options.seed = 1;
    options.canvas_type = CanvasType::Sdl;
    options.scale = 1;

    App app {options};
    app.run();

    // TODO Aggiungi asserzioni
    // Non so cosa testare. run() dura per sempre e non so come fermarlo
}