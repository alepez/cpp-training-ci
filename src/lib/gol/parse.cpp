#include "parse.hpp"

#include <cstdlib>

namespace alepez {

template<>
int parse_or_fallback<int>(const char* str, int fallback) {
    char* end = nullptr;
    int n = static_cast<int>(strtol(str, &end, 10));
    bool valid = *str != '\0' && *end == '\0';

    if (!valid) {
        return fallback;
    }

    return n;
}

}  // namespace alepez