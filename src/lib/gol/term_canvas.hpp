#pragma once

#include <memory>

#include "canvas.hpp"

namespace alepez {

class TermCanvas: public Canvas {
  public:
    TermCanvas(int width, int height, std::ostream& stream);
    TermCanvas(int width, int height);
    bool draw(const Grid& grid) override;

  private:
    int m_width;
    int m_height;
    std::ostream& m_stream;
};

}  // namespace alepez