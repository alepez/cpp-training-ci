#pragma once

#include <vector>

namespace alepez {

enum class CellState {
    Dead = 0,
    Alive = 1,
};

struct Cell {
    CellState state {CellState::Dead};
};

class Grid {
  public:
    explicit Grid(int width, int height) : m_width(width), m_height(height) {
        int cells_count = width * height;
        m_data.resize(static_cast<std::size_t>(cells_count));
    }

    Cell& at(int x, int y) {
        return m_data[index_of(x, y)];
    }

    [[nodiscard]] const Cell& at(int x, int y) const {
        return m_data[index_of(x, y)];
    }

    [[nodiscard]] int width() const {
        return m_width;
    }

    [[nodiscard]] int height() const {
        return m_height;
    }

    void swap(Grid& other) {
        m_data.swap(other.m_data);
    }

  private:
    [[nodiscard]] int index_of(int x, int y) const {
        return ((m_height + y) % m_height) * m_width
            + ((m_width + x) % m_width);
    }

    int m_width;
    int m_height;
    std::vector<Cell> m_data;
};

}  // namespace alepez