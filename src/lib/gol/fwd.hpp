#pragma once

namespace alepez {

class Grid;
class Canvas;
class Game;
struct Options;
enum class CanvasType;

}  // namespace alepez