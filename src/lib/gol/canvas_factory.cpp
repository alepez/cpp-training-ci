#include "canvas_factory.hpp"

#include <spdlog/spdlog.h>

#include "canvas.hpp"
#include "options.hpp"
#include "sdl_canvas.hpp"
#include "term_canvas.hpp"

namespace alepez {

std::unique_ptr<Canvas> make_canvas(const Options& options) {
    Canvas* canvas = nullptr;

    switch (options.canvas_type) {
        case CanvasType::Terminal:
            canvas = new TermCanvas(options.width, options.height);
            break;
#if FEATURE_SDL
        case CanvasType::Sdl:
            canvas =
                new SdlCanvas(options.width, options.height, options.scale);
            break;
#endif
        default:
            SPDLOG_ERROR("Canvas type not supported");
            break;
    }

    return std::unique_ptr<Canvas>(canvas);
}

}  // namespace alepez