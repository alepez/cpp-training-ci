#pragma once

namespace alepez {

template<typename T>
T parse_or_fallback(const char* str, T fallback);

template<typename T>
T parse_arg_or_fallback(int argc, const char** argv, int index, T fallback) {
    if (index >= argc) {
        return fallback;
    }

    return parse_or_fallback<T>(argv[index], fallback);
}

}  // namespace alepez