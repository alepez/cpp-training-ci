#if FEATURE_SDL

#include "sdl_canvas.hpp"

#include <SDL2/SDL.h>
#include <fmt/core.h>

#include "grid.hpp"

namespace alepez {

struct SdlCanvasError: std::exception {};

static int color(Cell cell) {
    bool alive = cell.state == CellState::Alive;
    return alive ? 0x00FFFFFF : 0x00000000;
}

SdlCanvas::SdlCanvas(int width, int height, int scale) {
    auto win_title = fmt::format("Conway's Game of Life");

    auto sdl_init_result = SDL_Init(SDL_INIT_VIDEO);

    if (sdl_init_result != 0) {
        throw SdlCanvasError {};
    }

    m_win = SDL_CreateWindow(
        win_title.c_str(),  //
        SDL_WINDOWPOS_UNDEFINED,  //
        SDL_WINDOWPOS_UNDEFINED,  //
        scale * width,  //
        scale * height,  //
        SDL_WINDOW_SHOWN  //
    );

    if (m_win == nullptr) {
        throw SdlCanvasError {};
    }
}

SdlCanvas::~SdlCanvas() {
    SDL_DestroyWindow(m_win);
    SDL_Quit();
}

bool SdlCanvas::draw(const Grid& grid) {
    auto surface = SDL_GetWindowSurface(m_win);
    auto pixels = static_cast<int*>(surface->pixels);

    int win_w = surface->w;
    int win_h = surface->h;

    draw_to_pixels(pixels, win_w, win_h, grid);

    SDL_UpdateWindowSurface(m_win);

    bool stop = false;

    while (true) {
        SDL_Event event;
        bool more_events = SDL_PollEvent(&event) == 1;

        if (!more_events) {
            break;
        }

        switch (event.type) {
            case SDL_QUIT:
                stop = true;
                break;
        }
    }

    return !stop;
}

void SdlCanvas::draw_to_pixels(
    int* pixels,
    int win_w,
    int win_h,
    const Grid& grid) {
    int grid_w = grid.width();
    int grid_h = grid.height();

    int scale_x = win_w / grid_w;
    int scale_y = win_h / grid_h;

    for (int win_x = 0; win_x < win_w; ++win_x) {
        for (int win_y = 0; win_y < win_h; ++win_y) {
            int grid_x = win_x / scale_x;
            int grid_y = win_y / scale_y;
            auto cell = grid.at(grid_x, grid_y);
            pixels[win_x + win_y * win_w] = color(cell);
        }
    }
}

}  // namespace alepez

#endif