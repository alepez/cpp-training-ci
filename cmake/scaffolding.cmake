function(add_module lib name)
    target_sources(${lib} PRIVATE ${name}.cpp)
    target_sources(${lib}_test PRIVATE ${name}_test.cpp)
endfunction()

function(add_catch2_executable name)
    add_executable(${name})

    target_link_libraries(${name}
            PRIVATE Catch2::Catch2WithMain
            PRIVATE ApprovalTests::ApprovalTests
    )

    add_test(NAME ${name} COMMAND ${name})
endfunction()

# As pedantic as possible (for Clang and GCC)
function(enable_pedantic_warnings target)
    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
        target_compile_options(${target} PRIVATE -Wall -Wextra -pedantic -Werror)
    endif ()
endfunction()

function(enable_code_coverage target)
    if (CMAKE_BUILD_TYPE STREQUAL "Coverage")
        target_compile_options(${target} PUBLIC --coverage)
        target_link_libraries(${target} PUBLIC --coverage)
    endif ()
endfunction()

function(enable_ubsan)
    if (CMAKE_BUILD_TYPE STREQUAL "UBSan")
        if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
            message(STATUS "Enabling Undefined Behavior Sanitizer")
            add_compile_options(
                    -fsanitize=undefined
                    -fsanitize=nullability
                    -fsanitize=float-divide-by-zero
                    -fsanitize=implicit-conversion
                    -fsanitize=local-bounds
                    -fno-sanitize-recover=all
            )
            add_link_options(-fsanitize=undefined)
        else ()
            message(WARNING "Undefined behavior sanitizer is only supported by Clang")
        endif ()
    endif ()
endfunction()

function(enable_asan)
    if (CMAKE_BUILD_TYPE STREQUAL "ASan")
        if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
            add_compile_options(-fsanitize=address)
            add_link_options(-fsanitize=address)
        else ()
            message(WARNING "Address sanitizer is only supported by Clang")
        endif ()
    endif ()
endfunction()

function(enable_msan)
    if (CMAKE_BUILD_TYPE STREQUAL "MSan")
        # Check if LLVM_MSAN is set
        # It is critical that you should build all the code in your program
        # (including libraries it uses, in particular, C++ standard library)
        # with MSan. See MemorySanitizerLibcxxHowTo for more details.
        # See https://github.com/google/sanitizers/wiki/MemorySanitizerLibcxxHowTo
        if (NOT DEFINED ENV{LLVM_MSAN})
            message(FATAL_ERROR "LLVM_MSAN environment variable is not set")
        elseif (NOT CMAKE_CXX_COMPILER_ID MATCHES "Clang")
            message(WARNING "Memory sanitizer is only supported by Clang")
        else ()
            add_compile_options(
                    -fsanitize=memory
                    -fsanitize-memory-track-origins
                    -fsanitize-ignorelist=${CMAKE_SOURCE_DIR}/.ignorelist.txt
                    -fPIE
                    -fno-omit-frame-pointer
                    -stdlib=libc++
                    -I$ENV{LLVM_MSAN}/include
                    -I$ENV{LLVM_MSAN}/include/c++/v1
            )
            add_link_options(
                    -fsanitize=memory
                    -fsanitize-memory-track-origins
                    -stdlib=libc++
                    -L$ENV{LLVM_MSAN}/lib
            )
        endif ()
    endif ()
endfunction()

function(enable_sanitizers target)
    enable_ubsan(${target})
    enable_asan(${target})
    enable_msan(${target})
endfunction()