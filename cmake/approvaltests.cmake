Include(FetchContent)

FetchContent_Declare(
        approvals-ApprovalTests
        URL https://github.com/approvals/ApprovalTests.cpp/archive/refs/tags/v.10.13.0.tar.gz
        URL_HASH SHA256=44896af00018fc051f0332d7a78e4b8caf4274d2e51e6ba786e6271575fb82c8
)

FetchContent_MakeAvailable(approvals-ApprovalTests)
