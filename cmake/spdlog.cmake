Include(FetchContent)

FetchContent_Declare(
        gabime-spdlog
        URL https://github.com/gabime/spdlog/archive/refs/tags/v1.14.1.tar.gz
        URL_HASH SHA256=1586508029a7d0670dfcb2d97575dcdc242d3868a259742b69f100801ab4e16b
)

FetchContent_MakeAvailable(gabime-spdlog)