cmake_minimum_required(VERSION 3.22)

if (${CMAKE_VERSION} VERSION_GREATER_EQUAL "3.0")
    cmake_policy(SET CMP0048 NEW)
endif ()

if (${CMAKE_VERSION} VERSION_GREATER_EQUAL "3.13")
    cmake_policy(SET CMP0077 NEW)
endif ()

if (${CMAKE_VERSION} VERSION_GREATER_EQUAL "3.24")
    cmake_policy(SET CMP0135 NEW)
endif ()

enable_testing()

project(game_of_life
        VERSION 0.1.0
        DESCRIPTION "Game of Life"
        LANGUAGES CXX
)

# Explicitly enable C++17 standard for the whole project
set(CMAKE_CXX_STANDARD 17)

# Some tools, like clang-tidy, need compile commands
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Project scaffolding
include(cmake/scaffolding.cmake)

enable_sanitizers(gol)

# Dependencies
include(cmake/catch2.cmake)
include(cmake/fmt.cmake)
include(cmake/spdlog.cmake)
include(cmake/approvaltests.cmake)

add_subdirectory(src)
add_subdirectory(tests)